package com.reservaja.reservaja.repository;

import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Mesa;



public interface MesaRepository extends CrudRepository<Mesa, String> {
	
	
}
