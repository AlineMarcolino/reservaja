package com.reservaja.reservaja.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Usuario;

public interface UsuarioRepository  extends CrudRepository<Usuario, Integer>{
	
	Optional<Usuario> findByEmail(String email);
	
}
