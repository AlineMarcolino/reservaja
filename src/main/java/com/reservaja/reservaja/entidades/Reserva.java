package com.reservaja.reservaja.entidades;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Reserva {

	@Id
	private int idReserva;
	private int idCliente;
	private int idMesa;
	private String horaInicio;
	private String horaFim;
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public int getIdMesa() {
		return idMesa;
	}
	public void setIdMesa(int idMesa) {
		this.idMesa = idMesa;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFim() {
		return horaFim;
	}
	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}
	

}
