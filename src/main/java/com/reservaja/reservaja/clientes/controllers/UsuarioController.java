package com.reservaja.reservaja.clientes.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.repository.UsuarioRepository;
import com.reservaja.reservaja.service.PasswordService;
import com.reservaja.reservaja.service.TokenService;

@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioQuery = usuarioRepository.findByEmail(usuario.getEmail());	
		
		if(! usuarioQuery.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioQuery.get().getSenha());
		
		if(deuCerto) {
			Usuario usuarioBanco = usuarioQuery.get();
			String token = tokenService.gerar(usuarioBanco.getId(), usuarioBanco.getPapel().toString());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			
			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
}