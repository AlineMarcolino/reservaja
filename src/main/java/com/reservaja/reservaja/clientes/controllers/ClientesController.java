package com.reservaja.reservaja.clientes.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reservaja.reservaja.entidades.Cliente;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.Papel;
import com.reservaja.reservaja.repository.ClienteRepository;
import com.reservaja.reservaja.repository.UsuarioRepository;
import com.reservaja.reservaja.service.PasswordService;

@Controller
public class ClientesController {
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PasswordService passwordService;
	
	
	@RequestMapping(path="/clientes", method=RequestMethod.GET)
	@ResponseBody 
	public Iterable<Cliente> getClientes() {		
		return clienteRepository.findAll();
	}
	
	@RequestMapping(path="/cliente/{email}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Cliente> getClienteByEmail(@PathVariable String email) {		
		Optional<Usuario> usuarioQuery = usuarioRepository.findByEmail(email);
		
		if(!usuarioQuery.isPresent()) {
			return null;
		}
		
		Usuario usuario = usuarioQuery.get();
		
		return clienteRepository.findByUsuario(usuario);
	}
	
	@RequestMapping(path="/cliente", method=RequestMethod.POST)
	@ResponseBody
	public Cliente createCliente(@RequestBody Cliente cliente) {		
		Usuario usuario = cliente.getUsuario();
		usuario.setSenha(passwordService.encode(usuario.getSenha()));
		usuario.setPapel(Papel.CLIENTE);
		usuario = usuarioRepository.save(usuario);
		
		cliente.setUsuario(usuario);
		
		return clienteRepository.save(cliente);
	}
}
